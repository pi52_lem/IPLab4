laba = {
    setNowDate: function () {
        var d = new Date();
        var options_1 = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
        };
        var date1 = document.getElementById('task1_string1');
        var date2 = document.getElementById('task1_string2');
        var date3 = document.getElementById('task1_string3');
        date1.innerHTML = d.toLocaleString("uk", options_1);
        date2.innerHTML = d.toLocaleString("uk", {weekday: 'long'});
        date3.innerHTML = d.getHours() + ':' + d.getMinutes();
    },
    date2: function (date) { // завдання 2
        var days_number = ['7', '1', '2', '3', '4', '5', '6'];
        var days_name = ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер', 'п\'ятниця', 'субота'];
        document.getElementById('task2_string1').innerHTML = days_number[date.getDay()];
        document.getElementById('task2_string2').innerHTML = days_name[date.getDay()];
    },
    daysAgo: function () { // завдання 3
        var days = document.getElementById('inputDate').value;
        var date = new Date();
        var options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
        };
        document.getElementById('task3_string0').innerHTML = date.toLocaleString("uk", options);
        if (days) {
            date.setDate(date.getDate() - parseInt(days));
            document.getElementById('task3_string1').innerHTML = date.toLocaleString("uk", options);
        }
    },
    daysInMonth: function () { // завдання 4
        var year = parseInt(document.getElementById('inputYear').value);
        var month = parseInt(document.getElementById('inputMonth').value);
        if (year && month) {
            var date = new Date(year, month, 0);
            document.getElementById('task4_string1').innerHTML = date.toLocaleString("uk", {day: 'numeric'});//date.getDate();
        }
    },
    getSecondBeforeAfter: function () { // завдання 5
        var now = new Date();
        var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        var diff = now - today;
        document.getElementById('task5_string1').innerHTML = Math.floor(diff / 1000);
        var tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
        var diff1 = tomorrow - now;
        document.getElementById('task5_string2').innerHTML = Math.floor(diff1 / 1000);
        return {
            today: Math.floor(diff / 1000),
            tomorrow: Math.floor(diff1 / 1000)
        };
    },
    getStringDate: function () { // завдання 6
        var dateInput = document.getElementById('t6_date').value;
        if (!dateInput)
            var date = new Date();
        else date = new Date(dateInput);
        var dd = date.getDate();
        if (dd < 10) dd = '0' + dd;
        var mm = date.getMonth() + 1;
        if (mm < 10) mm = '0' + mm;
        var yy = date.getFullYear() % 100;
        if (yy < 10) yy = '0' + yy;
        document.getElementById('task6_string1').innerHTML = dd + '.' + mm + '.' + yy;
    },
    getDiffDate: function () {// завдання 7
        var dateInput1 = document.getElementById('t7_date1').value;
        var date1 = new Date(dateInput1);
        var dateInput2 = document.getElementById('t7_date2').value;
        var date2 = new Date(dateInput2);
        delta = Math.abs(date2 - date1);
        document.getElementById('task7_string').innerHTML = Math.round(delta / 1000 / 60 / 60 / 24) + '(днів)';
    },
    formatDate: function () {// завдання 8
        var dateInput1 = document.getElementById('t8_date').value;
        var date1 = new Date(dateInput1);
        var res = '';
        var diff = new Date() - date1;
        if (diff < 1000) {
            res = 'только что';
        }
        if (res == '') {
            var sec = Math.floor(diff / 1000);
            if (sec < 60) {
                res = sec + ' сек. назад';
            }
        }
        if (res == '') {
            var min = Math.floor(diff / 60000);
            if (min < 60) {
                res = min + ' мин. назад';
            }
        }
        if (res == '') {
            var d = date1;
            d = [
                '0' + d.getDate(),
                '0' + (d.getMonth() + 1),
                '' + d.getFullYear(),
                '0' + d.getHours(),
                '0' + d.getMinutes()
            ];
            for (var i = 0; i < d.length; i++) {
                d[i] = d[i].slice(-2);
            }

            res = d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
        }
        document.getElementById('task8_string').innerHTML = res;
    },
    createDate: function () { // завдання 9
        var dateInput = document.getElementById('t9_date').value;
        var date;
        if (dateInput.indexOf('.') != -1) {
            var dd = dateInput.slice(0, dateInput.indexOf('.'));
            dateInput = dateInput.slice(dateInput.indexOf('.') + 1);
            var mm = dateInput.slice(0, dateInput.indexOf('.'));
            dateInput = dateInput.slice(dateInput.indexOf('.') + 1);
            var time = 0;
            var h = 0, i = 0, s = 0;
            if(dateInput.indexOf(' ') != -1) {
                var yy = dateInput.slice(0, dateInput.indexOf(' '));
                dateInput = dateInput.slice(dateInput.indexOf(' ') + 1);
                if(dateInput.indexOf(':') != -1) {
                    h = dateInput.slice(0, dateInput.indexOf(':'));
                    dateInput = dateInput.slice(dateInput.indexOf(':') + 1);
                    if(dateInput.indexOf(':') != -1) {
                        i = dateInput.slice(0, dateInput.indexOf(':'));
                        dateInput = dateInput.slice(dateInput.indexOf(':') + 1);
                        s = dateInput;
                    } else {
                        i = dateInput;
                    }
                } else {
                    h = dateInput;
                }
            } else {
                yy = dateInput;
            }
            date = new Date(yy, mm, dd, h, i, s);
        } else {
            date = new Date(dateInput);
        }
        alert(date);
    },
    getDateByCode: function () { // завдання 10
        var date = new Date();
        var options = {
            era: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            weekday: 'long',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };
        var code = document.getElementById('inputCodeDate').value;
        if (!code || code.length != 2)
            code = 'uk';
        document.getElementById('task10_string1').innerHTML = date.toLocaleString(code, options);
    }
};